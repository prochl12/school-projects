#ifndef __PROGTEST__
#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <climits>
#include <cfloat>
#include <cassert>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <numeric>
#include <vector>
#include <set>
#include <list>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <queue>
#include <stack>
#include <deque>
#include <memory>
#include <functional>
#include <thread>
#include <mutex>
#include <atomic>
#include <chrono>
#include <condition_variable>
#include "progtest_solver.h"
#include "sample_tester.h"
using namespace std;
#endif /* __PROGTEST__ */

const int c_debug = 0;

class CSentinelHacker
{
  public:
    static bool              SeqSolve                      ( const vector<uint64_t> & fragments,
                                                             CBigInt         & res );
    void                     AddTransmitter                ( ATransmitter      x );
    void                     AddReceiver                   ( AReceiver         x );
    void                     AddFragment                   ( uint64_t          x );
    void                     Start                         ( unsigned          thrCount );
    void                     Stop                          ( void );
    uint32_t                 getID                         ( uint64_t fragment );
    void                     recvFunc                      ( AReceiver rcv );
    void                     workerFunc                    ( void );
    void                     transFunc                     ( ATransmitter trn );
  private:
    //vector < uint64_t > & m_dump;
    bool                                                   m_inputDone = false;
    bool                                                   m_workDone = false;
    vector < ATransmitter >                                m_vTrans;
    vector < AReceiver >                                   m_vRecvs;
    vector < thread >                                      m_vRecvThrds;
    vector < thread >                                      m_vWorkerThrds;
    vector < thread >                                      m_vTranThrds;
    queue < uint64_t >                                     m_qFragments;
    queue < pair < uint32_t, CBigInt > >                   m_qSolutions;
    map < uint32_t, vector < uint64_t > >                  m_mFragments;
    mutex                                                  m_qFragmentsMtx;
    mutex                                                  m_qSolutionsMtx;
    mutex                                                  m_mFragmentsMtx;


};
// TODO: CSentinelHacker implementation goes here

bool CSentinelHacker :: SeqSolve ( const vector < uint64_t > & fragments, CBigInt & res )
{
  return ( FindPermutations ( fragments . data(), fragments . size(), [&] ( const uint8_t * data, size_t bits ) {
    auto expressionCount = CountExpressions ( data + 4, bits - 32 );
    if ( res . CompareTo ( expressionCount ) == -1 )
      res = expressionCount;
  }) != 0 );
}

void CSentinelHacker :: AddTransmitter ( ATransmitter x )
{
  m_vTrans . push_back ( x );
}

void CSentinelHacker :: AddReceiver ( AReceiver x )
{
  m_vRecvs . push_back ( x );
}

void CSentinelHacker :: AddFragment ( uint64_t x )
{
  if ( c_debug )
    cout << "[i] Adding fragment..." << endl;
  m_qFragments . push ( x );
}

void CSentinelHacker :: Start ( unsigned thrCount )
{
  if ( c_debug )
    cout << "[i] Starting with " << thrCount << " threads..." << endl;

  m_inputDone = false;
  m_workDone = false;
  for ( auto & re : m_vRecvs )
    m_vRecvThrds . emplace_back ( thread ( & CSentinelHacker :: recvFunc, this, re ) );
  for ( unsigned i = 0; i < thrCount; i++ )
    m_vWorkerThrds . emplace_back ( thread ( & CSentinelHacker :: workerFunc, this ) );
  for ( auto & tr : m_vTrans )
    m_vTranThrds . emplace_back ( thread ( & CSentinelHacker :: transFunc, this, tr ) );

  if ( c_debug )
    cout << "[i] Starting!" << endl;
}

void CSentinelHacker :: Stop ( void )
{
  if ( c_debug )
    cout << "[i] Stopping..." << endl;

  for ( auto & thrd : m_vRecvThrds )
    thrd . join();
  m_inputDone = true;

  if ( c_debug )
    cout << "[i] Recievers joined!" << endl;

  for ( auto & thrd : m_vWorkerThrds )
    thrd . join();
  m_workDone = true;

  if ( c_debug )
    cout << "[i] Workers joined!" << endl;

  for ( auto & thrd : m_vTranThrds )
    thrd . join();

  if ( c_debug )
    cout << "[i] Transmitters joined!" << endl;
}

uint32_t CSentinelHacker :: getID ( uint64_t fragment )
{
  return fragment >> 37;
}

void CSentinelHacker :: recvFunc ( AReceiver  rcv )
{
  uint64_t tmp;
  unique_lock < mutex > lck1 ( m_qFragmentsMtx, defer_lock );
  while ( rcv -> Recv ( tmp ) ) {
    if ( c_debug )
      cout << "[i] Recieving fragment..." << endl;
    lck1 . lock();
    m_qFragments . push ( tmp );
    lck1 . unlock();
    if ( c_debug )
      cout << "[i] Fragment recieved!" << endl;
  }
  if ( c_debug )
    cout << "[i] All fragments recieved! Stopping reciever!" << endl;
}

void CSentinelHacker :: workerFunc ( void )
{
  uint64_t tmpFrag;
  CBigInt tmpCnt;
  vector < uint64_t > tmpVector;
  unique_lock < mutex > lck1 ( m_qFragmentsMtx, defer_lock );
  unique_lock < mutex > lck2 ( m_mFragmentsMtx, defer_lock );
  unique_lock < mutex > lck3 ( m_qSolutionsMtx, defer_lock );
  while ( ! m_inputDone || ! m_qFragments . empty() )
  {
    lck1 . lock();
    if ( ! m_qFragments . empty() ) {
      tmpFrag = m_qFragments . front();
      m_qFragments . pop();
      lck1 . unlock();

      if ( c_debug )
        cout << "[i] Fragment accepted!" << endl;

      lck2 . lock();
      try {
        m_mFragments . at ( getID ( tmpFrag ) ) . push_back ( tmpFrag );
      } catch ( const out_of_range e ) {
        m_mFragments . insert ( pair < uint32_t, vector < uint64_t > > ( getID ( tmpFrag ), { tmpFrag } ) );
      }
      tmpVector = m_mFragments . at ( getID ( tmpFrag ) );
      lck2 . unlock();
      if ( c_debug )
        cout << "[i] Solving..." << endl;
      tmpCnt = 0;
      if ( SeqSolve ( tmpVector, tmpCnt ) ) {
        lck3 . lock();
          m_qSolutions . push ( make_pair ( getID ( tmpFrag ), tmpCnt ) );
        lck3 . unlock();

        if ( c_debug )
          cout << "[i] Message solved!" << endl;

        lck2 . lock();
        m_mFragments . erase ( getID ( tmpFrag ) );
        lck2 . unlock();
      }
      if ( c_debug )
        cout << "[i] Message couldn't be solved!" << endl;
      continue;
    }
    lck1 . unlock();
  }
}

void CSentinelHacker :: transFunc ( ATransmitter trn )
{
  uint32_t tmpID;
  CBigInt tmpCnt;
  unique_lock < mutex > lck1 ( m_qSolutionsMtx, defer_lock );
  unique_lock < mutex > lck2 ( m_mFragmentsMtx, defer_lock );
  while ( 69 ) {
    lck1 . lock ();
    if ( ! m_qSolutions . empty() ) {
      tmpID = m_qSolutions . front() . first;
      tmpCnt = m_qSolutions . front() . second;
      m_qSolutions . pop();
      lck1 . unlock();
      trn -> Send ( tmpID, tmpCnt );
    } else {
      lck1 . unlock();
      if ( m_workDone )
        break;
    }
  }

  lck2 . lock();
  for ( auto & x : m_mFragments ) {
    trn -> Incomplete ( x . first );
  }
  m_mFragments . clear();
  lck2 . unlock();
}

//-------------------------------------------------------------------------------------------------
#ifndef __PROGTEST__
int                main                                    ( void )
{

  if ( c_debug )
    cout << "[i] Running tests..." << endl;

  using namespace std::placeholders;
  for ( const auto & x : g_TestSets )
  {
    CBigInt res;
    assert ( CSentinelHacker::SeqSolve ( x . m_Fragments, res ) );
    assert ( CBigInt ( x . m_Result ) . CompareTo ( res ) == 0 );
  }

  if ( c_debug )
    cout << "[i] SeqSolve tests complete!" << endl;

  CSentinelHacker test;
  auto            trans = make_shared<CExampleTransmitter> ();
  AReceiver       recv  = make_shared<CExampleReceiver> ( initializer_list<uint64_t> { 0x02230000000c, 0x071e124dabef, 0x02360037680e, 0x071d2f8fe0a1, 0x055500150755 } );

  test . AddTransmitter ( trans );
  test . AddReceiver ( recv );
  test . Start ( 1 );

  static initializer_list<uint64_t> t1Data = { 0x071f6b8342ab, 0x0738011f538d, 0x0732000129c3, 0x055e6ecfa0f9, 0x02ffaa027451, 0x02280000010b, 0x02fb0b88bc3e };
  thread t1 ( FragmentSender, bind ( &CSentinelHacker::AddFragment, &test, _1 ), t1Data );

  static initializer_list<uint64_t> t2Data = { 0x073700609bbd, 0x055901d61e7b, 0x022a0000032b, 0x016f0000edfb };
  thread t2 ( FragmentSender, bind ( &CSentinelHacker::AddFragment, &test, _1 ), t2Data );
  FragmentSender ( bind ( &CSentinelHacker::AddFragment, &test, _1 ), initializer_list<uint64_t> { 0x017f4cb42a68, 0x02260000000d, 0x072500000025 } );

  if ( c_debug )
    cout << "[i] FragmentSenders bound!" << endl;

  t1 . join ();
  t2 . join ();

  if ( c_debug )
    cout << "[i] Main threads joined!" << endl;

  test . Stop ();
  assert ( trans -> TotalSent () == 4 );
  assert ( trans -> TotalIncomplete () == 2 );
  return 0;
}
#endif /* __PROGTEST__ */
