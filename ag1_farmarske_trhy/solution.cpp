//
//  v03 : Input works, BFS works, output is ready
//  Todo: Find food
//


#include <iostream>
#include <vector>
#include <queue>

using namespace std;

int main ( void ) {

//  Variable declaration

	unsigned n;					// towns / nodes
	unsigned m;					// roads / edges
	unsigned p;					// unique food types
	unsigned q;					// minimal food types required
	unsigned tmpSrc, tmpDst;     // temporary source and destination node
	unsigned totalCost = 0;      // total roads used

//  Input handeling

	cin >> n >> m;
	cin >> p >> q;

	// food types @ towns
	vector < bool > temp ( p, false );
	vector < vector < bool > > gotFoods ( n, temp );
	vector < int > foods ( n, -1 );
	vector < int > townCost ( n, 0 );
	// list of roads
	vector < vector < int > > roads ( n );

	// scan all foods
    for ( unsigned i = 0; i < n; i ++ ) {
        int tmp;
        cin >> tmp;
        foods . at ( i ) = tmp;
        gotFoods . at ( i ) . at ( tmp ) = true;
    }

    // scan all roads
    for ( unsigned i = 0; i < m; i ++ ) {
        cin >> tmpSrc >> tmpDst;
        roads . at ( tmpSrc ) . push_back ( tmpDst );
        roads . at ( tmpDst ) . push_back ( tmpSrc );
    }

//  Algorithm
    // runs BFS for each town
    for ( unsigned i = 0; i < n; i ++ ) {

        // BFS variables init
        queue < int > q1;
        vector < bool > isVisited ( n, false );
        vector < int > cost ( n, -1 );
        int cur;
        q1 . push ( i );
        cost . at ( i ) = 0;

        // main BFS loop
        unsigned ff = 1;     // food already found
        while ( ! q1 . empty () && ff < q ) {
            cur = q1 . front ();
            q1 . pop ();
            // for each neighbor
            for ( unsigned j = 0; j < roads . at ( cur ) . size () && ff < q; j++ ) {
                // if neighbor is not visited
                if ( ! isVisited . at ( roads . at ( cur ) . at ( j ) ) ) {
                    cost . at ( roads . at ( cur ) . at ( j ) ) = cost . at ( cur ) + 1;
                    isVisited . at ( roads . at ( cur ) . at ( j ) ) = true;
                    q1 . push ( roads . at ( cur ) . at ( j ) );
                    // if I want neighbor's food//cout << "Hmm, [" << i << "][" << foods . at ( roads . at ( cur ) . at ( j ) ) << "] = " << gotFoods . at ( i ) . at ( foods . at ( roads . at ( cur ) . at ( j ) ) ) << endl;
                    if ( ! gotFoods . at ( i ) . at ( foods . at ( roads . at ( cur ) . at ( j ) ) ) ) {//cout << "Add food " << foods . at ( roads . at ( cur ) . at ( j ) ) << " to town " << i << "." << endl;
                        gotFoods . at ( i ) . at ( foods . at ( roads . at ( cur ) . at ( j ) ) ) = true;
                        ff++;
                        townCost . at ( i ) += cost . at ( roads . at ( cur ) . at ( j ) );
                    }
                }
            }

            isVisited . at ( cur ) = true;
        }

        totalCost += townCost . at ( i );
    }

//  Output

    cout << totalCost << endl;

    for ( unsigned i = 0; i < n; i++ ) {
        cout << townCost . at ( i ) << " ";
        for ( unsigned j = 0; j < p; j++ ) {
            if ( gotFoods . at ( i ) . at ( j ) )
                cout << j << " ";
        }
        cout << endl;
    }


}
