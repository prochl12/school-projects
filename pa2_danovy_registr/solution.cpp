#ifndef __PROGTEST__
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cassert>
#include <cstring>
#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <memory>
#include <functional>
#include <stdexcept>
#include <algorithm>
using namespace std;
#endif /* __PROGTEST__ */

const int debug = 0;


// Class defunitions


class CPerson
{
	public:
										CPerson 									( string name,
																								string addr,
																								string account);
		bool  					GreaterThanByAcc  				( const CPerson & other ) const;
		bool  					GreaterThanByName 				( const CPerson & other ) const;
		string 					getName                   ( void ) const;
		string 					getAddr                   ( void ) const;
		string 					getAcc                    ( void ) const;
		int 						getIncome                 ( void ) const;
		int 						getExpense                ( void ) const;
		void 						Income                    ( int amount );
		void 						Expense                   ( int amount );
		void  					Print                     ( void ) const;
	protected:
		string          m_name;
		string          m_addr;
		string          m_account;
		int             m_income;
		int             m_expense;
};


class CIterator
{
  public:
														 CIterator										 ( const vector < shared_ptr < CPerson > > * dataVector, size_t startPos );
    bool                     AtEnd                         ( void ) const;
    void                     Next                          ( void );
    string                   Name                          ( void ) const;
    string                   Addr                          ( void ) const;
    string                   Account                       ( void ) const;
  protected:
		const vector < shared_ptr < CPerson > > * 		m_data;
    size_t 																				m_curr;
};


class CTaxRegister
{
  public:
    bool                     Birth                         ( const string    & name,
                                                             const string    & addr,
                                                             const string    & account );
    bool                     Death                         ( const string    & name,
                                                             const string    & addr );
    bool                     Income                        ( const string    & account,
                                                             int               amount );
    bool                     Income                        ( const string    & name,
                                                             const string    & addr,
                                                             int               amount );
    bool                     Expense                       ( const string    & account,
                                                             int               amount );
    bool                     Expense                       ( const string    & name,
                                                             const string    & addr,
                                                             int               amount );
    bool                     Audit                         ( const string    & name,
                                                             const string    & addr,
                                                             string          & account,
                                                             int             & sumIncome,
                                                             int             & sumExpense ) const;
    CIterator                ListByName                    ( void ) const;
    // own supporting methods
    CPerson *                FindByAcc                     ( const string    & account ) const;
    CPerson *                FindByName                    ( const string    & name,
                                                             const string    & addr ) const;
		size_t              		 GetIndexByAcc                 ( const string    & account ) const;
		size_t									 GetIndexByName                ( const string    & name,
																												     const string    & addr ) const;
    void                     Bubble                        ( void );
		void 										 Print 												 ( short unsigned int sortBy ) const;
		// constants
		static const unsigned short int DEFAULT = 0;
		static const unsigned short int ACC = 1;
		static const unsigned short int NAME = 2;
  protected:
    vector < shared_ptr < CPerson > >     m_byAcc;
    vector < shared_ptr < CPerson > >     m_byName;
};


// CPerson function implementation


CPerson :: CPerson ( string tmpName, string tmpAddr, string tmpAcc ) :
								   m_name ( tmpName ),
									 m_addr ( tmpAddr ),
									 m_account ( tmpAcc ),
									 m_income ( 0 ),
									 m_expense ( 0 ) {
	if ( debug ) {
		cout << "[i] Created CPerson \"" << m_name << "\"." << endl;
	}
}

string CPerson :: getName ( void ) const {
	return this -> m_name;
}

string CPerson :: getAddr ( void ) const {
	return this -> m_addr;
}

string CPerson :: getAcc ( void ) const {
	return this -> m_account;
}

int CPerson :: getIncome ( void ) const {
	return this -> m_income;
}

int CPerson :: getExpense ( void ) const {
	return this -> m_expense;
}

bool CPerson :: GreaterThanByAcc ( const CPerson & other ) const {
	return ( this -> m_account > other . m_account );
}

bool CPerson :: GreaterThanByName ( const CPerson & other ) const {
	if ( this -> m_name == other . m_name )
		return ( this -> m_addr > other . getAddr() );
	return ( this -> m_name > other . getName() );
}

void CPerson :: Income ( int amount ) {
	this -> m_income += amount;
}

void CPerson :: Expense ( int amount ) {
	this -> m_expense += amount;
}

void CPerson :: Print ( void ) const {
	cout << setfill ( ' ' ) << "| " << setw ( 15 ) << this ->m_name<< " | "
		   << setw ( 20 ) << this -> m_addr << " | "
			 << setw ( 15 ) << this -> m_account << " | "
			 << setw ( 5 ) << this -> m_income << " | "
			 << setw ( 5 ) << this -> m_expense << " |" << endl;
}


// CIterator function implementation


CIterator :: CIterator ( const vector < shared_ptr < CPerson > > * dataVector , size_t startPos ) :
											 m_data ( dataVector ), m_curr ( startPos ) { }

bool CIterator :: AtEnd ( void ) const
{
	return ( m_curr >= m_data -> size() );
}

void CIterator :: Next ( void )
{
	this -> m_curr ++;
}

string CIterator :: Name ( void ) const
{
	return this -> m_data -> at ( this -> m_curr ) . get() -> getName();
}

string CIterator :: Addr ( void ) const
{
	return this -> m_data -> at ( this -> m_curr ) . get() -> getAddr();
}

string CIterator :: Account ( void ) const
{
	return this -> m_data -> at ( this -> m_curr ) . get() -> getAcc();
}


// CTaxRegister function implementation


bool CTaxRegister :: Birth ( const string & name, const string & addr, const string & account )
{
  // check whether a person is already present
  if ( this -> FindByAcc ( account ) != nullptr ) {
    if ( debug ) {
      cout << "[!] Person with this account already exists." << endl;
    }
    return false;
  }

  if ( this -> FindByName ( name, addr ) != nullptr ) {
    if ( debug ) {
      cout << "[!] Person with this name & address already exists." << endl;
    }
    return false;
  }

  // insert new person into our database
	shared_ptr < CPerson > psn = make_shared < CPerson > ( CPerson ( name, addr, account ) );

  // insert person's reference to sorted lists
  this -> m_byAcc . push_back ( psn );
  this -> m_byName . push_back ( psn );

	if ( debug ) {
		this -> Print ( 0 );
	}

  // bubble to keep vectors sorted
  this -> Bubble();

	if ( debug ) {
		this -> Print ( this -> ACC );
		this -> Print ( this -> NAME );
	}

  return true;

}

bool CTaxRegister :: Death ( const string & name, const string & addr )
{

	CPerson * psn = this -> FindByName ( name, addr );

	if ( psn == nullptr ) {
		return false;
	}

	size_t iByAcc = this -> GetIndexByAcc ( psn -> getAcc() );
	size_t iByName = this -> GetIndexByName ( psn -> getName(), psn -> getAddr() );

	this -> m_byAcc . erase ( this-> m_byAcc . begin() + iByAcc );

	this -> m_byName . erase ( this-> m_byName . begin() + iByName );

	return true;

}

bool CTaxRegister :: Income ( const string & account, int amount )
{

  if ( debug ) {
    cout << ">> Income ( \"" << account << "\", " << amount << " );" << endl;
  }

  CPerson * psn = this -> FindByAcc ( account );

  if ( psn == nullptr ) {
    if ( debug ) {
      cout << "[!] Unknown account!" << endl;
    }
    return false;
  }

  psn -> Income ( amount );

  return true;
}

bool CTaxRegister :: Income ( const string & name, const string & addr, int amount )
{

	  if ( debug ) {
	    cout << ">> Income ( \"" << name << "\", " << addr << "\", " << amount << " );" << endl;
	  }

	  CPerson * psn = this -> FindByName ( name, addr );

	  if ( psn == nullptr ) {
	    if ( debug ) {
	      cout << "[!] Unknown account!" << endl;
	    }
	    return false;
	  }

	  psn -> Income ( amount );

	  return true;
}

bool CTaxRegister :: Expense ( const string & account, int amount )
{

	  if ( debug ) {
	    cout << ">> Expense ( \"" << account << "\", " << amount << " );" << endl;
	  }

	  CPerson * psn = this -> FindByAcc ( account );

	  if ( psn == nullptr ) {
	    if ( debug ) {
	      cout << "[!] Unknown account!" << endl;
	    }
	    return false;
	  }

	  psn -> Expense ( amount );

	  return true;
}

bool CTaxRegister :: Expense ( const string & name, const string & addr, int amount )
{

		  if ( debug ) {
		    cout << ">> Expense ( \"" << name << "\", " << addr << "\", " << amount << " );" << endl;
		  }

		  CPerson * psn = this -> FindByName ( name, addr );

		  if ( psn == nullptr ) {
		    if ( debug ) {
		      cout << "[!] Unknown account!" << endl;
		    }
		    return false;
		  }

		  psn -> Expense ( amount );

		  return true;
}

bool CTaxRegister :: Audit ( const string & name, const string & addr, string & account, int & sumIncome, int & sumExpense ) const
{
	CPerson * psn = this -> FindByName ( name, addr );

	if ( psn == nullptr ) {
		if ( debug ) {
			cout << "[!] Unknown account!" << endl;
		}
		return false;
	}

	account = psn -> getAcc();
	sumIncome = psn -> getIncome();
	sumExpense = psn -> getExpense();

	return true;

}

CIterator CTaxRegister :: ListByName ( void ) const
{
	return CIterator( & this -> m_byName, 0 );
}

CPerson * CTaxRegister :: FindByAcc ( const string & acc ) const
{

	if ( debug ) {
		cout << ">>> Searching for account \"" << acc << "\"..." << endl;
	}

  size_t curr = 0;
  size_t min = ( ( 1 < this -> m_byAcc . size() ) ? 1 : this -> m_byAcc . size() );
  while ( min < this -> m_byAcc . size()
          && this -> m_byAcc . at ( min ) -> getAcc() < acc )
  {
    curr = ( min + 1 );
    min = ( ( ( min * 2 + 1 ) < ( this -> m_byAcc . size() ) ) ? ( min * 2 + 1 ) : ( this -> m_byAcc . size() ) );
  }
  while ( curr < min ) {
    size_t testcurr = curr + ( ( min - curr ) >> 1 );
    if ( this -> m_byAcc . at ( testcurr ) -> getAcc() < acc ) {
      curr = ( testcurr + 1 );
    } else {
      min = testcurr;
    }
  }
  return ( ( ( curr < this -> m_byAcc . size() )
             && ( this -> m_byAcc . at ( curr ) -> getAcc() == acc ) )
           ? this -> m_byAcc . at ( curr ) . get() : nullptr );
}

CPerson * CTaxRegister :: FindByName ( const string & name, const string & addr ) const
{
  size_t curr = 0;
  size_t min = ( ( 1 < this -> m_byName . size() ) ? 1 : this -> m_byName . size() );
  while ( min < this -> m_byName . size()
          && ( ( this -> m_byName . at ( min ) -> getName() < name )
               || ( ( this -> m_byName . at ( min ) -> getName() == name )
                    && ( this -> m_byName . at ( min ) -> getAddr() < addr ) ) ) )
  {
    curr = ( min + 1 );
    min = ( ( ( min * 2 + 1 ) < ( this -> m_byName . size() ) ) ? ( min * 2 + 1 ) : ( this -> m_byName . size() ) );
  }
  while ( curr < min ) {
    size_t testcurr = curr + ( ( min - curr ) >> 1 );
    if ( ( this -> m_byName . at ( testcurr ) -> getName() < name )
         || ( ( this -> m_byName . at ( testcurr ) -> getName() == name )
              && ( this -> m_byName . at ( testcurr ) -> getAddr() < addr ) ) )
    {
      curr = ( testcurr + 1 );
    } else {
      min = testcurr;
    }
  }
  return ( ( ( curr < this -> m_byName . size() )
             && ( ( this -> m_byName . at ( curr ) -> getName() == name )
                  && ( this -> m_byName . at ( curr ) -> getAddr() == addr ) ) )
           ? this -> m_byName . at ( curr ) . get() : nullptr );
}

size_t CTaxRegister :: GetIndexByAcc ( const string & acc ) const
{

	if ( debug ) {
		cout << ">>> Searching for account \"" << acc << "\"..." << endl;
	}

  size_t curr = 0;
  size_t min = ( ( 1 < this -> m_byAcc . size() ) ? 1 : this -> m_byAcc . size() );
  while ( min < this -> m_byAcc . size()
          && this -> m_byAcc . at ( min ) -> getAcc() < acc )
  {
    curr = ( min + 1 );
    min = ( ( ( min * 2 + 1 ) < ( this -> m_byAcc . size() ) ) ? ( min * 2 + 1 ) : ( this -> m_byAcc . size() ) );
  }
  while ( curr < min ) {
    size_t testcurr = curr + ( ( min - curr ) >> 1 );
    if ( this -> m_byAcc . at ( testcurr ) -> getAcc() < acc ) {
      curr = ( testcurr + 1 );
    } else {
      min = testcurr;
    }
  }
  return ( ( ( curr < this -> m_byAcc . size() )
             && ( this -> m_byAcc . at ( curr ) -> getAcc() == acc ) )
           ? curr : 0 );
}

size_t CTaxRegister :: GetIndexByName ( const string & name, const string & addr ) const
{
  size_t curr = 0;
  size_t min = ( ( 1 < this -> m_byName . size() ) ? 1 : this -> m_byName . size() );
  while ( min < this -> m_byName . size()
          && ( ( this -> m_byName . at ( min ) -> getName() < name )
               || ( ( this -> m_byName . at ( min ) -> getName() == name )
                    && ( this -> m_byName . at ( min ) -> getAddr() < addr ) ) ) )
  {
    curr = ( min + 1 );
    min = ( ( ( min * 2 + 1 ) < ( this -> m_byName . size() ) ) ? ( min * 2 + 1 ) : ( this -> m_byName . size() ) );
  }
  while ( curr < min ) {
    size_t testcurr = curr + ( ( min - curr ) >> 1 );
    if ( ( this -> m_byName . at ( testcurr ) -> getName() < name )
         || ( ( this -> m_byName . at ( testcurr ) -> getName() == name )
              && ( this -> m_byName . at ( testcurr ) -> getAddr() < addr ) ) )
    {
      curr = ( testcurr + 1 );
    } else {
      min = testcurr;
    }
  }
  return ( ( ( curr < this -> m_byName . size() )
             && ( ( this -> m_byName . at ( curr ) -> getName() == name )
                  && ( this -> m_byName . at ( curr ) -> getAddr() == addr ) ) )
           ? curr : 0 );
}

void CTaxRegister :: Bubble ( void )
{
  // The vectors were sorted and one new element has been added to both of them.
  // We need to sort those to preserve the order:

  if ( debug ) {
    cout << ">>> Let's bubble!" << endl;
  }

  // for the m_byAcc vector
  size_t i = ( this -> m_byAcc . size() - 1 );

  if ( debug ) {
    cout << ">>>> i == " << i << endl;
  }

  while ( i > 0 ) {
    if ( m_byAcc . at ( i - 1 ) -> GreaterThanByAcc ( * m_byAcc . at ( i ) ) ) {
      if ( debug ) {
        cout << ">>>> swapping left in m_byAcc..." << endl;
      }
      swap ( m_byAcc . at ( i - 1 ), m_byAcc . at ( i ) );
    } else {
      break;
    }
    i--;
  }
  // for the m_byName vector
  i = ( this -> m_byName . size() - 1 );

  while ( i > 0 ) {
    if ( m_byName . at ( i - 1 ) -> GreaterThanByName ( * m_byName . at ( i ) ) ) {
      if ( debug ) {
        cout << ">>>> swapping left in m_byName..." << endl;
      }
      swap ( m_byName . at ( i ), m_byName . at ( i - 1 ) );
    } else {
      break;
    }
    i--;
  }
}

void CTaxRegister :: Print ( short unsigned int sortBy ) const
{

	const vector < shared_ptr < CPerson > > * data;

	switch ( sortBy ) {
		case this -> NAME: 	data = & m_byName; break;
		default:						data = & m_byAcc; break;
	}

	cout << "________________________________________________________________________________" << endl << endl;
	cout << "  # |      Name       |       Address        |     Account     |   +   |   -   |" << endl;
	cout << "_______________________________________________________________________________" << endl << endl;

	for ( size_t i = 0; i < data -> size(); i++ )
	{
		cout << setw ( 3 ) << i << " ";
		data -> at ( i ) . get() -> Print();
	}

	cout << "________________________________________________________________________________" << endl << endl;
}

// ---  ---  ---

#ifndef __PROGTEST__
int main ( void )
{
  string acct;
  int    sumIncome, sumExpense;
  CTaxRegister b1;
  assert ( b1 . Birth ( "John Smith", "Oak Road 23", "123/456/789" ) );
  assert ( b1 . Birth ( "Jane Hacker", "Main Street 17", "Xuj5#94" ) );
  assert ( b1 . Birth ( "Peter Hacker", "Main Street 17", "634oddT" ) );
  assert ( b1 . Birth ( "John Smith", "Main Street 17", "Z343rwZ" ) );
  assert ( b1 . Income ( "Xuj5#94", 1000 ) );
  assert ( b1 . Income ( "634oddT", 2000 ) );
  assert ( b1 . Income ( "123/456/789", 3000 ) );
  assert ( b1 . Income ( "634oddT", 4000 ) );
  assert ( b1 . Income ( "Peter Hacker", "Main Street 17", 2000 ) );
  assert ( b1 . Expense ( "Jane Hacker", "Main Street 17", 2000 ) );
  assert ( b1 . Expense ( "John Smith", "Main Street 17", 500 ) );
  assert ( b1 . Expense ( "Jane Hacker", "Main Street 17", 1000 ) );
  assert ( b1 . Expense ( "Xuj5#94", 1300 ) );
  assert ( b1 . Audit ( "John Smith", "Oak Road 23", acct, sumIncome, sumExpense ) );
  assert ( acct == "123/456/789" );
  assert ( sumIncome == 3000 );
  assert ( sumExpense == 0 );
  assert ( b1 . Audit ( "Jane Hacker", "Main Street 17", acct, sumIncome, sumExpense ) );
  assert ( acct == "Xuj5#94" );
  assert ( sumIncome == 1000 );
  assert ( sumExpense == 4300 );
  assert ( b1 . Audit ( "Peter Hacker", "Main Street 17", acct, sumIncome, sumExpense ) );
  assert ( acct == "634oddT" );
  assert ( sumIncome == 8000 );
  assert ( sumExpense == 0 );
  assert ( b1 . Audit ( "John Smith", "Main Street 17", acct, sumIncome, sumExpense ) );
  assert ( acct == "Z343rwZ" );
  assert ( sumIncome == 0 );
  assert ( sumExpense == 500 );
  CIterator it = b1 . ListByName ();
  assert ( ! it . AtEnd ()
           && it . Name () == "Jane Hacker"
           && it . Addr () == "Main Street 17"
           && it . Account () == "Xuj5#94" );
  it . Next ();
  assert ( ! it . AtEnd ()
           && it . Name () == "John Smith"
           && it . Addr () == "Main Street 17"
           && it . Account () == "Z343rwZ" );
  it . Next ();
  assert ( ! it . AtEnd ()
           && it . Name () == "John Smith"
           && it . Addr () == "Oak Road 23"
           && it . Account () == "123/456/789" );
  it . Next ();
  assert ( ! it . AtEnd ()
           && it . Name () == "Peter Hacker"
           && it . Addr () == "Main Street 17"
           && it . Account () == "634oddT" );
  it . Next ();
  assert ( it . AtEnd () );

  assert ( b1 . Death ( "John Smith", "Main Street 17" ) );

  CTaxRegister b2;
  assert ( b2 . Birth ( "John Smith", "Oak Road 23", "123/456/789" ) );
  assert ( b2 . Birth ( "Jane Hacker", "Main Street 17", "Xuj5#94" ) );
  assert ( !b2 . Income ( "634oddT", 4000 ) );
  assert ( !b2 . Expense ( "John Smith", "Main Street 18", 500 ) );
  assert ( !b2 . Audit ( "John Nowak", "Second Street 23", acct, sumIncome, sumExpense ) );
  assert ( !b2 . Death ( "Peter Nowak", "5-th Avenue" ) );
  assert ( !b2 . Birth ( "Jane Hacker", "Main Street 17", "4et689A" ) );
  assert ( !b2 . Birth ( "Joe Hacker", "Elm Street 23", "Xuj5#94" ) );
  assert ( b2 . Death ( "Jane Hacker", "Main Street 17" ) );
  assert ( b2 . Birth ( "Joe Hacker", "Elm Street 23", "Xuj5#94" ) );
  assert ( b2 . Audit ( "Joe Hacker", "Elm Street 23", acct, sumIncome, sumExpense ) );
  assert ( acct == "Xuj5#94" );
  assert ( sumIncome == 0 );
  assert ( sumExpense == 0 );
  assert ( !b2 . Birth ( "Joe Hacker", "Elm Street 23", "AAj5#94" ) );

  return 0;
}
#endif /* __PROGTEST__ */
