#include <stdio.h>

int main ( void ) {

	int base, hi, lo;
	unsigned curNum, zeroStreak, maxZeroStreak, numDigits, numZeros;
	char op, sep, first, loz;

	// INPUT

	printf ( "Zadejte intervaly:\n" );

	while ( 69 ) {

		base = hi = lo = curNum = zeroStreak = maxZeroStreak = numDigits = numZeros = op = 0;


		if ( scanf ( " %c", &first ) != 1 || !( first == '<' || first == 'r' ) ) {
			printf ( "Nespravny vstup.\n" );
			return 1;
		}

		if ( first == 'r') {
			if ( scanf ( " %d %c %c ", &base, &sep, &loz ) != 3 ||
				 base < 2 ||
				 base > 36 ||
				 sep != ':' ||
			   	 loz != '<' )
			{
				printf ( "Nespravny vstup.\n" );
				return 1;
			}
		} else {
			base = 10;
		}

		if ( scanf ( " %d ; %d > %c ", &lo, &hi, &op ) != 3 ||
			 lo > hi ||
			 !( op == 'l' || op == 's' || op == 'z' ) ||
			 lo < 0 ||
			 hi < 0 )
		{
			printf ( "Nespravny vstup.\n" );
			return 1;
		}


		for ( int i = lo; i <= hi; i++ ) {
			curNum = i;
			if ( curNum == 0 ) {
				numDigits++;
				numZeros++;
				zeroStreak++;
			}
			while ( curNum != 0 ) {
				if ( curNum % base == 0 ) {
					zeroStreak++;
					numZeros++;
				} else {
					if ( zeroStreak > maxZeroStreak )
						maxZeroStreak = zeroStreak;
					zeroStreak = 0;
				}
				numDigits++;
				curNum /= base;
			}

			if ( zeroStreak > maxZeroStreak )
				maxZeroStreak = zeroStreak;
		}

		switch ( op ) {
			case 'l':
				printf ( "Cifer: %d\n", numDigits );
				break;
			case 's':
				printf ( "Sekvence: %d\n", maxZeroStreak );
				break;
			case 'z':
				printf ( "Nul: %d\n", numZeros );
				break;
		}

		if ( feof ( stdin ) )
			break;

	}

	return 0;
}
